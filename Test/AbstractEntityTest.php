<?php
namespace Brown298\TestExtension\Test;

use Phake;
use DateTime;

/**
 * Class AbstractEntityTest
 *
 * @package Brown298\TestExtension\Test
 */
abstract class AbstractEntityTest extends AbstractTest
{
    /**
     * @var string name of the entity
     */
    public $entityName = '';

    /**
     * @var mixed the actual entity
     */
    public $entity;

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();
        $this->entity = new $this->entityName();
    }

    /**
     * testCreate
     */
    public function testCreate()
    {
        $this->assertInstanceOf($this->entityName, $this->entity);
    }

    /**
     * getSetProvider
     *
     * @return array
     */
    public function getSetProvider()
    {
        return array(
            $this->entityName.' id' => array('getId', 'id'),
        );
    }

    /**
     * testGetSet
     *
     * @dataProvider getSetProvider
     *
     * @param      $getFunction
     * @param      $setFunction
     * @param null $dataType
     */
    public function testGetSet($getFunction, $setFunction, $dataType = null)
    {
        $value = $this->getDataByType($dataType);

        if (method_exists($this->entity, $setFunction)) {
            $this->entity->$setFunction($value);
        } else {
            $this->setProtectedValue($this->entity, $setFunction, $value);
        }
        $this->assertSame(
            $value,
            $this->entity->$getFunction(),
            'Entity ' . $getFunction . '/' . $setFunction . ' failed for ' . $this->entityName
        );
    }

    /**
     * getDataByType
     *
     * creates test values for use in the get set testing
     *
     * @param null $dataType
     *
     * @return array|DateTime|string
     */
    protected function getDataByType($dataType = null)
    {
        if ($dataType !== null) {
            switch($dataType) {
                case 'array':
                    $value = array(
                        'testValue_' . rand(0,100)
                    );
                    break;
                case 'DateTime':
                    $value = new DateTime();
                    break;
                default:
                    $type = $dataType;
                    $value = new $type();
                    break;
            }
        } else {
            $value = 'testValue_' . rand(0,100);
        }

        return $value;
    }
} 