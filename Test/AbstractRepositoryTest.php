<?php
namespace Brown298\TestExtension\Test;

use Phake;

/**
 * Class AbstractRespositoryTest
 *
 * adds basic testing functionality for testing doctrine repositories
 *
 * @package Brown298\TestExtension\Test
 */
abstract class AbstractRepositoryTest extends AbstractTest
{
    /**
     * @Mock
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @Mock
     * @var \Doctrine\ORM\QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @Mock
     * @var \Doctrine\ORM\AbstractQuery
     */
    protected $query;

    /**
     * @Mock
     * @var \Doctrine\ORM\Mapping\ClassMetadata
     */
    protected $classMetaData;

    /**
     * @Mock
     * @var \Doctrine\ORM\UnitOfWork
     */
    protected $unitOfWork;

    /**
     * @Mock
     * @var \Doctrine\ORM\Persisters\BasicEntityPersister
     */
    protected $entityPersister;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $repositoryName;


    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();
        $this->repository = new $this->repositoryName($this->em, $this->classMetaData);
        Phake::when($this->em)->getUnitOfWork()->thenReturn($this->unitOfWork);
        Phake::when($this->unitOfWork)->getEntityPersister(Phake::anyParameters())->thenReturn($this->entityPersister);
        Phake::when($this->em)->createQueryBuilder(Phake::anyParameters())->thenReturn($this->queryBuilder);

        // add calls for chaining
        Phake::when($this->queryBuilder)->select(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->delete(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->join(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->from(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->where(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->leftJoin(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->innerJoin(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->andWhere(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->setParameter(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->addOrderBy(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->addGroupBy(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->orderBy(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->setMaxResults(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->distinct(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->having(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->getQuery()->thenReturn($this->query);

    }

    /**
     * testCreate
     *
     * ensure we get a valid object
     */
    public function testCreate()
    {
        $this->assertInstanceOf($this->repositoryName, $this->repository);
    }


} 