<?php
namespace Brown298\TestExtension\Test;

use Phake;

/**
 * Class AbstractDoctrineFunctionTest
 *
 * creates the mocks necessary to test doctrine functions
 *
 * @package Brown298\TestExtension\Test
 */
abstract class AbstractDoctrineFunctionTest extends AbstractTest
{
    /**
     * @Mock
     * @var \Doctrine\ORM\Query\Parser
     */
    protected $parser;

    /**
     * @Mock
     * @var \Doctrine\ORM\Query\SqlWalker
     */
    protected $sqlWalker;

    /**
     * @Mock
     * @var \Doctrine\DBAL\Connection
     */
    protected $connection;

    /**
     * @Mock
     * @var \Doctrine\DBAL\Platforms\AbstractPlatform
     */
    protected $databasePlatform;

    /**
     * @var
     */
    protected $node;

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->sqlWalker)->getConnection()->thenReturn($this->connection);
        Phake::when($this->connection)->getDatabasePlatform()->thenReturn($this->databasePlatform);
    }

} 