<?php
namespace Brown298\TestExtension\Test;

use Phake;

/**
 * Class AbstractFormTest
 *
 * @package Brown298\TestExtension\Test
 */
abstract class AbstractFormTest extends AbstractTest
{
    /**
     * @var string name of the class
     */
    protected $className = '';

    /**
     * @var Form
     */
    public $form;

    /**
     * @Mock
     * @var \Symfony\Component\Form\FormBuilderInterface
     */
    public $formBuilderInterface;

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * testCreate
     */
    public function testCreate()
    {
        $this->assertInstanceOf($this->className, $this->form);
    }

    /**
     * testGetName
     */
    public function testGetName()
    {
        $this->assertTrue(is_string($this->form->getName()), "Form name should be a string");

    }

    /**
     * testBuildForm
     */
    public function testBuildForm()
    {
        Phake::when($this->formBuilderInterface)->add(Phake::anyParameters())
            ->thenReturn($this->formBuilderInterface);
        $this->form->buildForm($this->formBuilderInterface, array());
        Phake::verify($this->formBuilderInterface, Phake::atLeast(1))->add(Phake::anyParameters());
    }
} 