<?php
namespace Brown298\TestExtension\Test;

use Phake;

/**
 * Class AbstractDocumentRepositoryTest
 *
 * @package Brown298\TestExtension\Test
 */
abstract class AbstractDocumentRepositoryTest extends AbstractTest
{
    /**
     * @Mock
     * @var \Doctrine\ODM\MongoDB\Query\Builder
     */
    protected $queryBuilder;

    /**
     * @Mock
     * @var \Doctrine\ODM\MongoDB\Query\Query
     */
    protected $query;

    /**
     * @Mock
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;

    /**
     * @Mock
     * @var \Doctrine\ODM\MongoDB\UnitOfWork
     */
    protected $uow;

    /**
     * @Mock
     * @var \Doctrine\ODM\MongoDB\Mapping\ClassMetadata
     */
    protected $classMeta;

    /**
     * @var string
     */
    protected $repositoryName;

    /**
     * @var \Doctrine\ODM\MongoDB\DocumentRepository
     */
    protected $repository;

    /**
     * @Mock
     * @var \Doctrine\MongoDB\Database
     */
    protected $documentDatabase;

    /**
     * setUp
     *
     */
    public function setUp()
    {
        parent::setUp();
        $this->repository = Phake::partialMock($this->repositoryName, $this->dm, $this->uow, $this->classMeta);
        Phake::when($this->dm)->getUnitOfWork()->thenReturn($this->uow);
        Phake::when($this->dm)->createQueryBuilder(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->dm)->getDocumentDatabase(Phake::anyParameters())->thenReturn($this->documentDatabase);

        Phake::when($this->queryBuilder)->field(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->equals(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->gte(Phake::anyParameters())->thenReturn($this->queryBuilder);
        Phake::when($this->queryBuilder)->lte(Phake::anyParameters())->thenReturn($this->queryBuilder);

        Phake::when($this->queryBuilder)->getQuery()->thenReturn($this->query);
    }
} 