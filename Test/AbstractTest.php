<?php
namespace Brown298\TestExtension\Test;

use \Phake;
use \PHPUnit_Framework_TestCase;

/**
 * Class AbstractTest
 *
 * Base test class that sets up our testing extension and adds simplified reflection functionality
 *
 */
abstract class AbstractTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * setUp
     */
    protected function setUp()
    {
        if (interface_exists('\Symfony\Component\DependencyInjection\ContainerInterface')) {
            $this->container = Phake::Mock('\Symfony\Component\DependencyInjection\ContainerInterface');
        }
        parent::setUp();
        Phake::initAnnotations($this);
    }

    /**
     * getProtectedValue
     *
     * gets a protected property's value
     *
     * @param mixed $object
     * @param string $property
     * @return mixed
     */
    public function getProtectedValue($object, $property)
    {
        $refl = new \ReflectionObject($object);
        $property = $refl->getProperty($property);
        $property->setAccessible(true);
        return $property->getValue($object);
    }


    /**
     * setProtectedValue
     *
     * sets a protected property's value
     *
     * @param mixed $object
     * @param string $property
     * @param mixed $value
     */
    public function setProtectedValue($object, $property, $value)
    {
        $refl = new \ReflectionObject($object);
        $property = $refl->getProperty($property);
        $property->setAccessible(true);
        $property->setValue($object, $value);
    }

    /**
     * callProtected
     *
     * calls a protected method on an object
     *
     * @param mixed $object
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function callProtected($object, $method, array $args = array())
    {
        $refl = new \ReflectionObject($object);
        $method = $refl->getMethod($method);
        $method->setAccessible(true);
        $result = $method->invokeArgs(
            $object,
            $args
        );
        return $result;
    }

    /**
     * generateAnonymousClass
     *
     * @param string                                   $className
     * @param \Brown298\TestExtension\Test\type|string $methodName
     *
     * @return type
     */
    public function generateAnonymousClass($className = 'testClass', $methodName = 'testMethod')
    {
        if(!class_exists($className)) {
            eval("class $className{ public function $methodName() {} };");
        }
        $object = new $className();
        return $object;
    }
} 