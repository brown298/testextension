<?php
namespace Brown298\TestExtension\Test;

use Phake;

/**
 * Class AbstractControllerTest
 *
 * @package Brown298\TestExtension\Test
 */
class AbstractControllerTest extends AbstractTest
{
    /**
     * @Mock
     * @var \Symfony\Component\Form\FormFactory
     */
    protected $formFactory;

    /**
     * @Mock
     * @var \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected $router;

    /**
     * @Mock
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @Mock
     * @var \Symfony\Component\Form\Form
     */
    protected $form;

    /**
     * @Mock
     * @var \Symfony\Component\HttpKernel\KernelInterface
     */
    protected $kernel;

    /**
     * @Mock
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    protected $session;

    /**
     * @Mock
     * @var \Symfony\Component\HttpFoundation\Session\Flash\FlashBag
     */
    protected $flashBag;

    /**
     * @Mock
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    /**
     * @Mock
     * @var \Symfony\Component\Validator\ValidatorInterface
     */
    protected $validator;

    /**
     * @Mock
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @Mock
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @Mock
     * @var \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
     */
    protected $renderer;

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();
        Phake::when($this->container)->get('router')->thenReturn($this->router);
        Phake::when($this->container)->get('request')->thenReturn($this->request);
        Phake::when($this->request)->duplicate(Phake::anyParameters())->thenReturn($this->request);
        Phake::when($this->container)->get('http_kernel')->thenReturn($this->kernel);
        Phake::when($this->container)->get('session')->thenReturn($this->session);
        Phake::when($this->container)->get('templating')->thenReturn($this->renderer);
        Phake::when($this->container)->get('form.factory')->thenReturn($this->formFactory);
        Phake::when($this->container)->get('validator')->thenReturn($this->validator);
        Phake::when($this->container)->get('logger')->thenReturn($this->logger);
        Phake::when($this->container)->get('templating')->thenReturn($this->renderer);
        Phake::when($this->container)->get('event_dispatcher')->thenReturn($this->eventDispatcher);
        Phake::when($this->formFactory)->create(Phake::anyParameters())->thenReturn($this->form);
        Phake::when($this->session)->getFlashBag()->thenReturn($this->flashBag);
        Phake::when($this->form)->get(Phake::anyParameters())->thenReturn($this->form);
    }

    /**
     * assertRedirect
     *
     * asserts the object is a redirectResponse
     *
     * @param mixed $object
     */
    protected function assertRedirect($object)
    {
        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $object);
    }

    /**
     * assertJsonResponse
     *
     * @param $object
     */
    protected function assertJsonResponse($object)
    {
        $this->assertInstanceOf('Symfony\Component\HttpFoundation\JsonResponse', $object);
    }
} 